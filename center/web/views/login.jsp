<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
</head>
<body>
<div style="height: 30px">
    ${error}
</div>
<form action="/login" method="post">
    <label>
        <input type="text" name="account" placeholder="账号">
    </label>
    <br/>
    <label>
        <input type="password" name="password" placeholder="密码">
    </label>
    <br/>
    <input type="submit" value="登录">
</form>
</body>
</html>
