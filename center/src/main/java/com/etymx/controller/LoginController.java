package com.etymx.controller;

import com.etymx.entity.User;
import com.etymx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    @GetMapping("/login")
    public String toLogin(String url, HttpServletRequest request, HttpSession session) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                User user = loginService.getUser(cookie.getValue());
                if (user != null) { // 当前用户已登录, 则直接返回token给系统
                    String server = getServer(url);
                    return "redirect:" + server + "success?url=" + url + "&token=" + cookie.getValue();
                } else {
                    break;
                }
            }
        }

        session.setAttribute("redirect", url);

        return "login";
    }

    @PostMapping("/login")
    public String login(String account, String password, RedirectAttributes flash,
                        HttpSession session, HttpServletResponse response) {
        if (account != null && password != null) {
            String token = loginService.login(account, password);
            if (token == null) { // 账号或密码错误
                flash.addFlashAttribute("error", "用户或密码错误!");
                return "redirect:/login";

            } else { // 登录成功
                Cookie cookie = new Cookie("token", token);
                // 时间尽量设置长一点, 使用redis的过期时间让登录令牌失效
                cookie.setMaxAge(60 * 60 * 24 * 7);
                response.addCookie(cookie);

                String url = (String) session.getAttribute("redirect");
                session.removeAttribute("redirect");
                String server = getServer(url);
                return "redirect:" + server + "success?url=" + url + "&token=" + token;
            }
        } else { // 数据不完整
            return "redirect:/views/login.jsp";
        }
    }

    @RequestMapping("/logout")
    public String logout(String url, String token, HttpSession session, HttpServletResponse response) {
        if (url == null) {
            url = "/";
        }
        if (token == null) {
            token = "";
        }
        session.removeAttribute("token");
        session.removeAttribute("user");
        loginService.logout(token);

        return "redirect:" + url;
    }

    private String getServer(String url) {
        if (url == null) {
            url = "";
        }
        Pattern pattern = Pattern.compile("(http://.*(:\\d+)?/).*");
        Matcher matcher = pattern.matcher(url);
        String server = "http://www.baidu.com/";
        if (matcher.matches()) {
            server = matcher.group(1);
        }

        return server;
    }
}
