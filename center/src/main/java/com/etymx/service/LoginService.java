package com.etymx.service;

import com.etymx.entity.User;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
public interface LoginService {

    String login(String account, String password);

    void logout(String token);

    User getUser(String token);
}
