package com.etymx.service.impl;

import com.etymx.dao.UserDao;
import com.etymx.entity.User;
import com.etymx.service.LoginService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.UUID;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    private UserDao userDao;

    @Autowired
    private JedisPool pool;

    @Override
    public String login(String account, String password) {
        User user = userDao.getUser(account);
        if (user == null || !user.getPassword().equals(password)) {
            return null;
        } else {
            String token = UUID.randomUUID().toString();
            Jedis jedis = pool.getResource();
            Gson gson = new Gson();
            jedis.set(token, gson.toJson(user));
            jedis.close();
            return token;
        }
    }

    @Override
    public void logout(String token) {
        try (Jedis jedis = pool.getResource()) {
            jedis.del(token);
        }
    }

    @Override
    public User getUser(String token) {
        if (token == null) {
            token = "";
        }

        try (Jedis jedis = pool.getResource()) {
            if (jedis.exists(token)) {
                Gson gson = new Gson();
                return gson.fromJson(jedis.get(token), User.class);
            }

            return null;
        }
    }
}
