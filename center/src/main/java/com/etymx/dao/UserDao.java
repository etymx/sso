package com.etymx.dao;

import com.etymx.entity.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
@Repository
public class UserDao {

    private final Map<String, User> users;

    // 静态账号数据
    public UserDao() {
        users = new HashMap<>();
        users.put("zhangsan", new User("zhangsan", "张三", "zhangsan"));
        users.put("lisi", new User("lisi", "李四", "lisi"));
        users.put("wangwu", new User("wangwu", "王五", "wangwu"));
    }

    public User getUser(String account) {
        return users.get(account);
    }


}
