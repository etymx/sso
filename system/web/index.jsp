<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
Hello! ${user.username}<br/>
<%
    if (session.getAttribute("user") == null) {
%>
<a href="/login">登录</a>
<%
} else {
%>
<a href="/logout">注销</a>
<%
    }
%>
<a href="/test">登录后才可查看(测试用)</a>
</body>
</html>
