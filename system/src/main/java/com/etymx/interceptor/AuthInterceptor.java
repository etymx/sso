package com.etymx.interceptor;

import com.etymx.entity.User;
import com.etymx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author LiangYongjie
 * @date 2018-07-27
 */
@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private LoginService loginService;

    /**
     * 测试权限拦截
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute("token");
        User user = loginService.getUser(token);
        if (user == null) {
            StringBuilder sb = new StringBuilder();
            String path = sb.append("http://").append(request.getServerName())
                .append(":").append(request.getServerPort()).append(request.getServletPath()).toString();
            response.sendRedirect("/login?url=" + path);
            return false;
        }

        return true;
    }
}
