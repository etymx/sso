package com.etymx.controller;

import com.etymx.entity.User;
import com.etymx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
@Controller
public class LoginController {

    @Value("${port}")
    private String port;

    @Value("${ip}")
    private String ip;

    @Autowired
    private LoginService loginService;

    @RequestMapping("/test")
    public void test(HttpServletResponse response) {
        try {
            response.setContentType("text/html;charset=utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("测试拦截后的效果用<br/>");
            writer.println("<a href='/'>返回首页</a>");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 登录成功后借此接口保存返回的token
     */
    @RequestMapping("/success")
    public String loginSuccess(String url, String token, HttpSession session) {
        session.setAttribute("token", token);
        return "redirect:/login?url=" + url;
    }


    @RequestMapping("/login")
    public String login(HttpServletRequest request, String url) {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute("token");

        if (token == null) {
            token = "";
        }
        User user = loginService.getUser(token);
        if (user != null) {
            if (url == null) {
                url = "/";
            }
            session.setAttribute("user", user);
            return "redirect:" + url;
        }

        // 如果指定了跳转路径, 且路径不为'/login', 则指向跳转路径, 否则指向根目录
        // 对访问路径进行权限拦截时, 未登录跳转到该方法可附上被拦截的路径, 在登录成功后可直接返回
        if (url == null || url.contains("/login")) {
            StringBuilder sb = new StringBuilder();
            url = sb.append("http://").append(request.getServerName()).append(":").append(request.getServerPort())
                .append("/").toString();
        }

        return "redirect:http://" + ip + ":" + port + "/login?url=" + url;
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute("token");
        if (token != null) {
            session.removeAttribute("user");
            session.removeAttribute("token");
            StringBuilder sb = new StringBuilder();
            sb.append("http://").append(request.getServerName()).append(":")
                .append(request.getServerPort());

            return "redirect:http://" + ip + ":" + port + "/logout?url=" + sb.toString()
                + "&token=" + token;
        } else {
            return "redirect:/";
        }

    }
}
