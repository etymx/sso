package com.etymx.service;

import com.etymx.entity.User;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
public interface LoginService {

    User getUser(String token);
}
