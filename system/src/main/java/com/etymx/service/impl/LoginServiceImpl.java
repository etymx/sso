package com.etymx.service.impl;

import com.etymx.entity.User;
import com.etymx.service.LoginService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author LiangYongjie
 * @date 2018-07-23
 */
@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    private JedisPool pool;

    @Override
    public User getUser(String token) {
        if (token == null) {
            token = "";
        }

        try (Jedis jedis = pool.getResource()) {
            if (jedis.exists(token)) {
                Gson gson = new Gson();
                return gson.fromJson(jedis.get(token), User.class);
            }

            return null;
        }
    }
}
